import { EmployeeController } from './employee.controller';
import { SyncCommandDispatcher } from '../../common/commands';
import { CreateEmployeeRequest } from './requests/create-employee-request.interface';
import { UpdateEmployeeRequest } from './requests/update-employee-request.interface';
import { EmployeeRepository } from './repositories/employees.repository';

describe('Employee Controller', () => {
  // Arrange
  const MockEmployeeRepository = jest.fn<EmployeeRepository, []>();

  let employeeRepository;
  let employeeController;

  // @ts-ignore
  const fakeCommandDispatcher: SyncCommandDispatcher = {
    // @ts-ignore
    execute: jest.fn(),
  };

  employeeRepository = new MockEmployeeRepository();

  employeeController = new EmployeeController(
    fakeCommandDispatcher,
    employeeRepository,
  );

  describe('When an employee is posted to the controller', () => {
    it('Should dispatch create employee command', async () => {
      // Arrange
      const request: CreateEmployeeRequest = {
        accountNumber: 'test',
        address: 'San Pedro Sula, Calle 1, Casa 5',
        bankName: 'Promerica',
        birthdate: '1993-14-04',
        city: 'San Pedro Sula',
        country: 'Honduras',
        displayName: 'Jimmy',
        effectiveDate: '2018-22-10',
        email: 'jimmyramos@acklenavenue.com',
        firstName: 'Jimmy',
        gender: 'Male',
        lastName: 'Ramos',
        middleName: 'Josue',
        personalEmail: 'jimmybanegas93@gmail.com',
        phoneNumber: '50494621230',
        region: 'Cortes',
        salary: '10',
        salaryType: 'Montly',
        secondLastName: 'Banegas',
        startDate: '2019-22-02',
        tags: 'Developer',
      };

      // Act
      await employeeController.createEmployee(request);

      // Assert
      expect(fakeCommandDispatcher.execute).toBeCalledWith(request);
    });
  });

  describe('when an employee UPDATE is sent to the controller', () => {
    it('should dispatch update employee command', async () => {
      // Arrange
      const request: UpdateEmployeeRequest = {
        employeeId: 100,
        accountNumber: 'Test',
        address: 'San Pedro Sula, Calle 5, Casa 1',
        bankName: 'Promerica',
        birthdate: '1993-14-04',
        city: 'San Pedro Sula',
        country: 'Honduras',
        displayName: 'Jimmy',
        effectiveDate: '2018-22-10',
        email: 'jimmyramos@acklenavenue.com',
        firstName: 'Jimmy',
        lastName: 'Ramos',
        middleName: 'Josue',
        personalEmail: 'jimmybanegas93@gmail.com',
        phoneNumber: '50494621230',
        region: 'Cortes',
        salary: '10',
        salaryType: 'Montly',
        secondLastName: 'Banegas',
        tags: 'Developer',
      };

      // Act
      await employeeController.updateEmployee(request);

      // Assert
      expect(fakeCommandDispatcher.execute).toHaveBeenCalledWith(request);
    });
  });

  describe('when a DELETE request is sent to the controller', () => {
    it('should disptach delete employee command', async () => {
      // Arrange
      const employeeId = { employeeId: 10 };

      // Act
      await employeeController.removeEmployee(employeeId.employeeId);

      // Assert
      expect(fakeCommandDispatcher.execute).toBeCalledWith({ employeeId: 10 });
    });
  });
});
