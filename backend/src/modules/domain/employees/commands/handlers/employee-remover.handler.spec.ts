import { EmployeeRepository } from '../../repositories/employees.repository';
import { RemoveEmployee } from '../remove-employee.command';
import { EmployeeRemover } from '../handlers/employee-remover.handler';

describe('Employee Remover', () => {
  describe('when a user removes an employee', () => {
    const MockEmployeeRepository = jest.fn<EmployeeRepository, []>(
      () =>
        ({
          findById: jest.fn().mockResolvedValue([]),
          remove: jest.fn(),
        } as any),
    );

    const employeeRepository = new MockEmployeeRepository();

    it('should remove the employee from the repository', async () => {
      // Arrange
      const handler = new EmployeeRemover(employeeRepository);

      const params = {
        employeeId: 100,
      };

      const removeEmployeeCommand = new RemoveEmployee(params.employeeId);

      // Act
      await handler.handle(removeEmployeeCommand);

      // Assert
      expect(employeeRepository.findById).toBeCalledWith(100);
      expect(employeeRepository.remove).toBeCalled();
    });
  });
});
