import { EmployeeCreator } from './employee-creator.handler';
import { EmployeeUpdater } from './employee-updater.handler';
import { EmployeeRemover } from './employee-remover.handler';

export const CommandHandlers = [
  EmployeeCreator,
  EmployeeUpdater,
  EmployeeRemover,
];
