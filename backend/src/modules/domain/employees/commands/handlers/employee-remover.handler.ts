import { RemoveEmployee } from '../remove-employee.command';
import { BaseCommandHandler } from '../../../../common/commands';
import { CommandHandler } from '@nestjs/cqrs';
import { Injectable } from '@nestjs/common';
import { EmployeeRepository } from '../../repositories/employees.repository';

@CommandHandler(RemoveEmployee)
@Injectable()
export class EmployeeRemover extends BaseCommandHandler<RemoveEmployee, void> {
  constructor(private readonly employeeRepository: EmployeeRepository) {
    super();
  }

  async handle(command: RemoveEmployee): Promise<void> {
    const { employeeId } = command;

    const employee = await this.employeeRepository.findById(employeeId);

    await this.employeeRepository.remove(employee);
  }
}
