import { ICommand } from '../../../common/commands';

export class RemoveEmployee implements ICommand {
  employeeId: number;

  constructor(employeeId: number) {
    this.employeeId = employeeId;
  }
}
