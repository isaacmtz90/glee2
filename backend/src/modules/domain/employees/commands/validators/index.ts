import { CreateEmployeeCompositeValidator } from './forCreateEmployee/create-employee-composite.validator';
import { CheckPropertiesValue } from './forCreateEmployee/check-properties-value.validator';
import { CheckUpdatePropertiesValue } from './forUpdateEmployee/check-properties-value.validator';
import { CheckEmployeeExists } from './forUpdateEmployee/check-employee-exists.validator';
import { CheckEmployeeExistsOnRemove } from './forRemoveEmployee/check-employee-exists.validator';
import { UpdateEmployeeCompositeValidator } from './forUpdateEmployee/update-employee-composite.validator';
import { RemoveEmployeeCompositeValidator } from './forRemoveEmployee/remove-employee-composite.validator';

export const CommandValidators = [
  CheckPropertiesValue,
  CreateEmployeeCompositeValidator,
  CheckUpdatePropertiesValue,
  CheckEmployeeExists,
  UpdateEmployeeCompositeValidator,
  CheckEmployeeExistsOnRemove,
  RemoveEmployeeCompositeValidator,
];
