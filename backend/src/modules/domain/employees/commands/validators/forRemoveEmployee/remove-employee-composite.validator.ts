import { CommandValidator } from '../../../../../common/commands/validation';
import { CompositeValidator } from '../../../../../common/commands/validation/CompositeValidator';
import { RemoveEmployee } from '../../remove-employee.command';
import { CheckEmployeeExistsOnRemove } from './check-employee-exists.validator';

@CommandValidator(RemoveEmployee)
export class RemoveEmployeeCompositeValidator extends CompositeValidator<
  RemoveEmployee
> {
  constructor(checkEmployeeExists: CheckEmployeeExistsOnRemove) {
    super([checkEmployeeExists]);
  }
}
