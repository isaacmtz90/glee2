import { RemoveEmployee } from '../../remove-employee.command';
import { EmployeeRepository } from '../../../repositories/employees.repository';
import { CheckEmployeeExistsOnRemove } from './check-employee-exists.validator';

describe('Check if employee exists', () => {
  const MockEmployeeRepository = jest.fn<EmployeeRepository, []>(
    () =>
      ({
        where: jest.fn().mockReturnValue({
          get: jest.fn().mockResolvedValue([]),
        }),
      } as any),
  );

  describe('when dispatching an update employee command', () => {
    it('should validate that the product exists in the db', async () => {
      // Arrange
      const employeeValidator = new CheckEmployeeExistsOnRemove(
        new MockEmployeeRepository(),
      );

      const params = {
        employeeId: 100,
      };

      // Act
      const removeEmployee = new RemoveEmployee(params.employeeId);
      const result = await employeeValidator.validate(removeEmployee);

      // Assert
      expect(result.hasError).toBeTruthy();
      expect(result.errors).toMatchObject([
        {
          field: 'employeeId',
          fieldLabel: 'employeeId',
          message: 'The employee does not exist',
          value: 100,
        },
      ]);
    });
  });
});
