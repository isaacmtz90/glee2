import { UpdateEmployee } from '../../update-employee.command';
import { JoiCommandValidator } from '../../../../../common/commands/validation';

import * as joi from 'joi';

export class CheckUpdatePropertiesValue extends JoiCommandValidator<
  UpdateEmployee
> {
  getSchema(command: UpdateEmployee) {
    return joi.object({
      employeeId: joi.number().required(),
      firstName: joi.string().required(),
      middleName: joi
        .string()
        .allow('')
        .optional(),
      lastName: joi.string().required(),
      secondLastName: joi
        .string()
        .allow('')
        .optional(),
      displayName: joi
        .string()
        .allow('')
        .optional(),
      email: joi
        .string()
        .email()
        .required(),
      personalEmail: joi
        .string()
        .allow('')
        .email()
        .optional(),
      birthdate: joi
        .string()
        .isoDate()
        .required(),
      address: joi
        .string()
        .allow('')
        .optional(),
      phoneNumber: joi
        .string()
        .allow('')
        .optional(),
      bankName: joi
        .string()
        .allow('')
        .optional(),
      accountNumber: joi
        .string()
        .allow('')
        .optional(),
      tags: joi
        .string()
        .allow('')
        .optional(),
      country: joi.string().required(),
      region: joi.string().required(),
      city: joi.string().required(),
      salary: joi
        .string()
        .regex(/^\d+$/)
        .required(),
      effectiveDate: joi
        .string()
        .isoDate()
        .required(),
      salaryType: joi.string().required(),
    });
  }
}
