import { CommandValidator } from '../../../../../common/commands/validation';
import { CompositeValidator } from '../../../../../common/commands/validation/CompositeValidator';
import { UpdateEmployee } from '../../update-employee.command';
import { CheckEmployeeExists } from './check-employee-exists.validator';
import { CheckUpdatePropertiesValue } from './check-properties-value.validator';

@CommandValidator(UpdateEmployee)
export class UpdateEmployeeCompositeValidator extends CompositeValidator<
  UpdateEmployee
> {
  constructor(
    joiValidator: CheckUpdatePropertiesValue,
    checkEmployeeExists: CheckEmployeeExists,
  ) {
    super([joiValidator, checkEmployeeExists]);
  }
}
