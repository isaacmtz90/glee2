import { UpdateEmployee } from '../../update-employee.command';
import { CheckUpdatePropertiesValue } from './check-properties-value.validator';

describe('Update Employee Validator', () => {
  describe('when sending an update employee command', () => {
    it('should pass the validation if the command is correct', async () => {
      // Arrange
      const updateEmployeeValidator = new CheckUpdatePropertiesValue();
      const params = {
        employeeId: 10,
        displayName: 'Test display',
        accountNumber: 'test',
        address: 'San Pedro Sula, Calle 1, Casa 5',
        birthdate: '2008-09-15T15:53:00',
        city: 'San Pedro Sula',
        country: 'Honduras',
        effectiveDate: '2008-09-15T15:53:00',
        email: 'jimmyramos@acklenavenue.com',
        firstName: 'Jimmy',
        lastName: 'Ramos',
        personalEmail: 'jimmybanegas93@gmail.com',
        phoneNumber: '50494621230',
        region: 'Cortes',
        salary: '10',
        salaryType: 'Montly',
        secondLastName: 'Banegas',
        tags: 'Developer',
        bankName: 'Promerica',
        middleName: 'test',
      };

      // Act
      const updateEmployee = new UpdateEmployee(params.employeeId, params.firstName, params.middleName,
        params.lastName, params.secondLastName, params.displayName, params.email,
        params.personalEmail, params.birthdate, params.address,
        params.phoneNumber, params.bankName, params.accountNumber,
        params.tags, params.country, params.region, params.city, params.salary,
        params.effectiveDate, params.salaryType);
      const result = await updateEmployeeValidator.validate(updateEmployee);

      // Assert
      expect(result.hasError).toBeFalsy();
      expect(result.errors.length).toBe(0);
    });
  });
});
