import { UpdateEmployee } from '../../update-employee.command';
import { EmployeeRepository } from '../../../repositories/employees.repository';
import { CheckEmployeeExists } from './check-employee-exists.validator';

describe('Check if employee exists', () => {
  const MockEmployeeRepository = jest.fn<EmployeeRepository, []>(
    () =>
      ({
        where: jest.fn().mockReturnValue({
          get: jest.fn().mockResolvedValue([]),
        }),
      } as any),
  );

  describe('when dispatching an update employee command', () => {
    it('should validate that the product exists in the db', async () => {
      // Arrange
      const employeeValidator = new CheckEmployeeExists(
        new MockEmployeeRepository(),
      );

      const params = {
        employeeId: 100,
        displayName: 'Test display',
        accountNumber: 'test',
        address: 'San Pedro Sula, Calle 1, Casa 5',
        birthdate: '2008-09-15T15:53:00',
        city: 'San Pedro Sula',
        country: 'Honduras',
        effectiveDate: '2008-09-15T15:53:00',
        email: 'jimmyramos@acklenavenue.com',
        firstName: 'Jimmy',
        lastName: 'Ramos',
        personalEmail: 'jimmybanegas93@gmail.com',
        phoneNumber: '50494621230',
        region: 'Cortes',
        salary: '10',
        salaryType: 'Montly',
        secondLastName: 'Banegas',
        tags: 'Developer',
        bankName: 'Promerica',
        middleName: 'test',
      };

      // Act
      const updateEmployee = new UpdateEmployee(params.employeeId, params.firstName, params.middleName,
        params.lastName, params.secondLastName, params.displayName, params.email,
        params.personalEmail, params.birthdate, params.address,
        params.phoneNumber, params.bankName, params.accountNumber,
        params.tags, params.country, params.region, params.city, params.salary,
        params.effectiveDate, params.salaryType);
      const result = await employeeValidator.validate(updateEmployee);

      // Assert
      expect(result.hasError).toBeTruthy();
      expect(result.errors).toMatchObject([
        {
          field: 'employeeId',
          fieldLabel: 'employeeId',
          message: 'The employee does not exist',
          value: 100,
        },
      ]);
    });
  });
});
