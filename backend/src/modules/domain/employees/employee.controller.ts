import {
  Controller,
  Post,
  UseGuards,
  Body,
  Get,
  Query,
  Param,
  Put,
  Delete,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreateEmployeeRequest } from './requests/create-employee-request.interface';
import { SyncCommandDispatcher } from '../../common/commands';
import { CreateEmployee } from './commands/create-employee.command';
import { UpdateEmployee } from './commands/update-employee.command';
import { RemoveEmployee } from './commands/remove-employee.command';
import { PaginatedEmployeeQuery } from './requests/paginated-employee-query.interface';
import { EmployeeRepository } from './repositories/employees.repository';
import { Usr } from '../../auth/user.decorator';
import { User } from '../../auth/user.interface';
import { Employee } from './entities/employee.entity';
import { UpdateEmployeeRequest } from './requests/update-employee-request.interface';

@Controller('/employees')
@UseGuards(AuthGuard())
export class EmployeeController {
  constructor(
    private readonly commandDispatcher: SyncCommandDispatcher,
    private readonly employeeRepository: EmployeeRepository,
  ) { }

  @Post()
  async createEmployee(
    @Body() employeeRequest: CreateEmployeeRequest,
  ): Promise<void> {
    await this.commandDispatcher.execute(new CreateEmployee(employeeRequest.firstName,
      employeeRequest.middleName, employeeRequest.lastName, employeeRequest.secondLastName,
      employeeRequest.displayName, employeeRequest.email, employeeRequest.personalEmail,
      employeeRequest.birthdate, employeeRequest.startDate, employeeRequest.address,
      employeeRequest.phoneNumber, employeeRequest.bankName, employeeRequest.accountNumber,
      employeeRequest.gender, employeeRequest.tags, employeeRequest.country, employeeRequest.region,
      employeeRequest.city, employeeRequest.salary, employeeRequest.effectiveDate,
      employeeRequest.salaryType));
  }

  @Get()
  async getEmployees(
    @Usr() user: User,
    @Query() query: PaginatedEmployeeQuery,
  ) {
    return this.employeeRepository
      .where({ id: query.id })
      .relation(query.relations)
      .paginate(query.page, query.perPage);
  }

  @Get(':id')
  async getById(@Param('id') employeeId: number) {
      const respose = await this.employeeRepository.findById(employeeId);
      return respose as Employee;
  }

  @Put()
  async updateEmployee(
    @Body() employeeRequest: UpdateEmployeeRequest,
  ): Promise<void> {
    await this.commandDispatcher.execute(new UpdateEmployee(employeeRequest.employeeId,
      employeeRequest.firstName, employeeRequest.middleName, employeeRequest.lastName,
      employeeRequest.secondLastName, employeeRequest.displayName, employeeRequest.email,
      employeeRequest.personalEmail, employeeRequest.birthdate, employeeRequest.address,
      employeeRequest.phoneNumber, employeeRequest.bankName, employeeRequest.accountNumber,
      employeeRequest.tags, employeeRequest.country, employeeRequest.region, employeeRequest.city,
      employeeRequest.salary, employeeRequest.effectiveDate, employeeRequest.salaryType));
  }

  @Delete(':id')
  async removeEmployee(@Param('id') employeeId: number) {
    await this.commandDispatcher.execute(new RemoveEmployee(employeeId));
  }
}
