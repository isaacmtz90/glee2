export interface UpdateEmployeeRequest {
  employeeId: number;
  firstName: string;
  middleName: string;
  lastName: string;
  secondLastName: string;
  displayName: string;
  email: string;
  personalEmail: string;
  birthdate: string;
  address: string;
  phoneNumber: string;
  bankName: string;
  accountNumber: string;
  tags: string;
  country: string;
  region: string;
  city: string;
  salary: string;
  effectiveDate: string;
  salaryType: string;
}
