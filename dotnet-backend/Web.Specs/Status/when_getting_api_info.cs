using System;
using dotenv.net;
using Machine.Specifications;
using Web.Infrastructure;
using Web.Status;

namespace Web.Specs.Status
{
    public class when_getting_api_info
    {
        static IApiInformationGetter _systemUnderTest;
        static ApiInformation _result;
        
        Establish context = () =>
        {
            _systemUnderTest = new DefaultApiInformationGetter();
        };

        Because of = () => { _result = _systemUnderTest.GetInfo(); };

        It should_return_information_about_the_api = () => { _result.Version.ShouldEqual("1.0.0.0"); };
    }
}