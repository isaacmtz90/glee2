using System.Collections.Generic;
using Domain.Employees;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Responses;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_getting_all_employees_by_parameter : given_an_employee_controller_context
    {
        static IList<EmployeeResponse> _expectedResponse;
        static IEnumerable<EmployeeResponse> _result;
        static Dictionary<string, string> _parameters;

        Establish _context = () =>
        {
            var employees = Builder<Employee>.CreateListOfSize(2).Build();

            _expectedResponse = Builder<EmployeeResponse>.CreateListOfSize(2).Build();

            _parameters = new Dictionary<string, string> {{"lastName", "Johnson"}};

            var queryable = employees.AsAsyncQueryable();
            Mock.Get(_readOnlyRepository).Setup(x => x.Find(_parameters)).Returns(queryable);

            Mock.Get(_mapper)
                .Setup(
                    x =>
                        x.Map<IEnumerable<Employee>, IEnumerable<EmployeeResponse>>(queryable))
                .Returns(_expectedResponse);
        };

        Because of = async () => { _result = await _employeesController.GetAll(_parameters); };

        It should_return_all_existing_employee = () => { _result.Should().Equal(_expectedResponse); };
    }
}