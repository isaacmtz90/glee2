using System;
using Domain.Employees.Commands;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Requests;
using Web.Employees.Responses;
using It = Machine.Specifications.It;
using ItVerify = Moq.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_creating_an_employee : given_an_employee_controller_context
    {
        static CreateEmployeeRequest _createCreateEmployeeRequest;
        static CreateEmployee _expectedCommand;
        static Guid _generatedId;
        static NewEmployeeResponse _result;

        Establish _context = () =>
        {
            _createCreateEmployeeRequest = new CreateEmployeeRequest
            {
                FirstName = "Test First Name",
                DisplayName = "Test Display Name",
                LastName = "Test Last Name",
                Gender = "Test Gender",
                Salary = 12.2,
                AccountNumber = "5005213434",
                Address = "Test Address",
                BankName = "Promerica",
                Birthdate = DateTime.Parse("1993-01-01"),
                City = "San Pedro Sula",
                Tags = "Developer",
                Email = "test@gmail.com",
                Region = "Cortés",
                Country = "Honduras",
                StartDate = DateTime.Parse("2018-01-01"),
                MiddleName = "Super",
                SalaryType = "Hourly",
                PhoneNumber = "2580-2204",
                EffectiveDate = DateTime.Parse("2018-01-01"),
                PersonalEmail = "personal@gmail.com",
                SecondLastName = "Testing"
            };

            _generatedId = Guid.NewGuid();
            Mock.Get(_identityGenerator).Setup(x => x.Generate()).Returns(_generatedId);

            _expectedCommand = new CreateEmployee(_generatedId, _createCreateEmployeeRequest.FirstName,
                _createCreateEmployeeRequest.DisplayName, _createCreateEmployeeRequest.LastName,
                _createCreateEmployeeRequest.Gender,
                _createCreateEmployeeRequest.Salary, _createCreateEmployeeRequest.MiddleName,
                _createCreateEmployeeRequest.SecondLastName,
                _createCreateEmployeeRequest.Email, _createCreateEmployeeRequest.PersonalEmail,
                _createCreateEmployeeRequest.Birthdate,
                _createCreateEmployeeRequest.StartDate, _createCreateEmployeeRequest.Address,
                _createCreateEmployeeRequest.PhoneNumber,
                _createCreateEmployeeRequest.BankName, _createCreateEmployeeRequest.AccountNumber,
                _createCreateEmployeeRequest.Tags,
                _createCreateEmployeeRequest.Country, _createCreateEmployeeRequest.Region,
                _createCreateEmployeeRequest.City,
                _createCreateEmployeeRequest.EffectiveDate,
                _createCreateEmployeeRequest.SalaryType);
        };

        Because of = async () => _result = await _employeesController.CreateEmployee(_createCreateEmployeeRequest);

        It should_dispatch_create_employee_command = () =>
        {
            Mock.Get(_commandDispatcher).Verify(dispatcher =>
                dispatcher.Dispatch(ItVerify.Is<CreateEmployee>(createEmployeeCommand =>
                    createEmployeeCommand.ShouldBeEquivalent(_expectedCommand))));
        };

        It should_return_the_new_employees_id = () => _result.Id.ShouldEqual(_generatedId);
    }
}