using System;
using System.Collections.Generic;
using AcklenAvenue.Commands;
using AutoMapper;
using Domain.Common.Repositories;
using Domain.Common.Services;
using Domain.Employees;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using Web.Employees.Responses;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_getting_all_employees
    {
        static EmployeesController _employeesController;
        static IReadOnlyRepository<Employee, Guid> _readOnlyRepository;
        static IMapper _mapper;
        static ICommandDispatcher _commandDispatcher;
        static IList<EmployeeResponse> _expectedResponse;
        static IEnumerable<EmployeeResponse> _result;

        Establish _context = () =>
        {
            _readOnlyRepository = Mock.Of<IReadOnlyRepository<Employee, Guid>>();
            _mapper = Mock.Of<IMapper>();
            _commandDispatcher = Mock.Of<ICommandDispatcher>();

            var employees = Builder<Employee>.CreateListOfSize(2).Build();

            _expectedResponse = Builder<EmployeeResponse>.CreateListOfSize(2).Build();

            var queryable = employees.AsAsyncQueryable();
            Mock.Get(_readOnlyRepository).Setup(x => x.Set()).Returns(queryable);
            Mock.Get(_mapper)
                .Setup(
                    x =>
                        x.Map<IEnumerable<Employee>, IEnumerable<EmployeeResponse>>(queryable))
                .Returns(_expectedResponse);

            _employeesController = new EmployeesController(_commandDispatcher, _readOnlyRepository, _mapper,
                Mock.Of<IIdentityGenerator<Guid>>());
        };

        Because of = async () => { _result = await _employeesController.GetAll(new Dictionary<string, string>()); };

        It should_return_all_existing_employee = () => { _result.Should().Equal(_expectedResponse); };
    }
}