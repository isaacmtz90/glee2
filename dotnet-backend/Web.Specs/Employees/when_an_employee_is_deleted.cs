using System;
using AcklenAvenue.Commands;
using AutoMapper;
using Domain.Common.Repositories;
using Domain.Common.Services;
using Domain.Employees;
using Domain.Employees.Commands;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using Web.Employees;
using It = Machine.Specifications.It;
using ItVerify = Moq.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_an_employee_is_deleted
    {
        static IMapper _mapper;
        static IReadOnlyRepository<Employee, Guid> _readOnlyRepository;
        static ICommandDispatcher _commandDispatcher;
        static EmployeesController _employeesController;
        static RemoveEmployee _expectedCommand;
        static Guid _employeeId;

        Establish context = () =>
        {
            _commandDispatcher = Mock.Of<ICommandDispatcher>();
            _readOnlyRepository = Mock.Of<IReadOnlyRepository<Employee, Guid>>();
            _mapper = Mock.Of<IMapper>();

            _employeesController = new EmployeesController(_commandDispatcher, _readOnlyRepository, _mapper, Mock.Of<IIdentityGenerator<Guid>>());

            _employeeId = Guid.NewGuid();

            _expectedCommand = new RemoveEmployee(_employeeId);
        };

        Because of = () => { _employeesController.DeleteEmployee(_employeeId).Await(); };

        It should_dispatch_delete_employee_command = () =>
        {
            Mock.Get(_commandDispatcher).Verify(dispatcher =>
                dispatcher.Dispatch(ItVerify.Is<RemoveEmployee>(deleteEmployeeCommand =>
                    _expectedCommand.ShouldBeEquivalent(deleteEmployeeCommand))));
        };
    }
}