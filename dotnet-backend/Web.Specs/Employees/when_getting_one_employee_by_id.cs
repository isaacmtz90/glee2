using System;
using Domain.Employees;
using FizzWare.NBuilder;
using FluentAssertions;
using Machine.Specifications;
using Moq;
using Web.Employees;
using Web.Employees.Responses;
using It = Machine.Specifications.It;

namespace Web.Specs.Employees
{
    [Subject(typeof(EmployeesController))]
    public class when_getting_one_employee_by_id : given_an_employee_controller_context
    {
        static Guid _id;
        static EmployeeResponse _result;
        static EmployeeResponse _expectedResponse;

        Establish _context = () =>
        {
            _id = Guid.NewGuid();

            var employee = Builder<Employee>.CreateNew()
                .With(x => x.Id, _id).Build();

            Mock.Get(_readOnlyRepository).Setup(x => x.GetById(Moq.It.Is<Guid>(i => i == _id))).ReturnsAsync(employee);

            _expectedResponse = Builder<EmployeeResponse>.CreateNew().Build();

            Mock.Get(_mapper)
                .Setup(
                    x =>
                        x.Map<Employee, EmployeeResponse>(employee))
                .Returns(_expectedResponse);
        };

        Because of = async () => { _result = await _employeesController.GetOne(_id); };

        It should_return_the_matching_employee = () => { _result.Should().Be(_expectedResponse); };
    }
}