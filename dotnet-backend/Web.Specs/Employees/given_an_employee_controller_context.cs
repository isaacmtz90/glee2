using System;
using AcklenAvenue.Commands;
using AutoMapper;
using Domain.Common.Repositories;
using Domain.Common.Services;
using Domain.Employees;
using Machine.Specifications;
using Moq;
using Web.Employees;

namespace Web.Specs.Employees
{
    public class given_an_employee_controller_context
    {
        protected static EmployeesController _employeesController;
        protected static ICommandDispatcher _commandDispatcher;
        protected static IReadOnlyRepository<Employee, Guid> _readOnlyRepository;
        protected static IMapper _mapper;
        protected static IIdentityGenerator<Guid> _identityGenerator;

        Establish _context = () =>
        {
            _commandDispatcher = Mock.Of<ICommandDispatcher>();
            _readOnlyRepository = Mock.Of<IReadOnlyRepository<Employee, Guid>>();
            _mapper = Mock.Of<IMapper>();
            _identityGenerator = Mock.Of<IIdentityGenerator<Guid>>();

            _employeesController =
                new EmployeesController(_commandDispatcher, _readOnlyRepository, _mapper, _identityGenerator);
        };
    }
}