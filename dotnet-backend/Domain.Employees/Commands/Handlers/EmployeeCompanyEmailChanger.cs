using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Common.Repositories;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeCompanyEmailChanger:ICommandHandler<ChangeEmployeeCompanyEmail>
    {
        readonly IWritableRepository<Employee, Guid> _repo;

        public EmployeeCompanyEmailChanger(IWritableRepository<Employee,Guid> repo)
        {
            _repo = repo;
        }

        public async Task Handle(ChangeEmployeeCompanyEmail command)
        {
            var employee = await _repo.GetById(command.Id);
            employee.ChangeCompanyEmail(command.Email);
            await _repo.Update(employee);
        }
    }
}