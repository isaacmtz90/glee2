using System;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Common.Repositories;

namespace Domain.Employees.Commands.Handlers
{
    public class EmployeeRemover : ICommandHandler<RemoveEmployee>
    {
        readonly IWritableRepository<Employee, Guid> _writableRepository;

        public EmployeeRemover(IWritableRepository<Employee, Guid> writableRepository)
        {
            _writableRepository = writableRepository;
        }

        public async Task Handle(RemoveEmployee command)
        {
            var employee = await _writableRepository.GetById(command.Id);
            employee.Remove();
            await _writableRepository.Delete(employee);
        }
    }
}