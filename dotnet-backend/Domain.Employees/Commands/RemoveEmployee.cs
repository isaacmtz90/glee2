using System;

namespace Domain.Employees.Commands
{
    public class RemoveEmployee
    {
        protected RemoveEmployee()
        {
            
        }

        public Guid Id { get; private set; } = Guid.Empty;

        public RemoveEmployee(Guid id)
        {
            Id = id;
        }
    }
}