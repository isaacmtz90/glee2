using System;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeCompanyEmail
    {
        public Guid Id { get; }
        public string Email { get; }

        public ChangeEmployeeCompanyEmail(Guid id, string email)
        {
            Id = id;
            Email = email;           
        }
    }
}