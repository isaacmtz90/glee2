using System;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeTags
    {
        public Guid Id { get; }
        public string Tags { get; }

        public ChangeEmployeeTags(Guid id, string tags)
        {
            Id = id;
            Tags = tags;
        }
    }
}