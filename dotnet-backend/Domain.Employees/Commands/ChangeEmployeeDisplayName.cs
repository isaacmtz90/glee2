using System;

namespace Domain.Employees.Commands
{
    public class ChangeEmployeeDisplayName
    {
        protected ChangeEmployeeDisplayName()
        {
        }

        public Guid Id { get; protected set; } = Guid.Empty;
        public string DisplayName { get; protected set; } = "";

        public ChangeEmployeeDisplayName(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }
    }
}