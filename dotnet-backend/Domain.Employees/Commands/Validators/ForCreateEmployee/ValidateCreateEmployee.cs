using Domain.Common;
using FluentValidation;

namespace Domain.Employees.Commands.Validators.ForCreateEmployee
{
    public class ValidateCreateEmployee : CommandValidatorBase<CreateEmployee>
    {
        public ValidateCreateEmployee()
        {
            RuleFor(employee => employee.FirstName).NotEmpty();
            RuleFor(e => e.LastName).NotEmpty();
            RuleFor(e => e.CompanyEmail).NotEmpty();
        }
    }
}