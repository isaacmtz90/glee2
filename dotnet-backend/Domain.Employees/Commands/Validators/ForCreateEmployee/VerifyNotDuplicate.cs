using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using Domain.Common.Exceptions;
using Domain.Common.Repositories;
using FluentValidation.Results;

namespace Domain.Employees.Commands.Validators.ForCreateEmployee
{
    public class VerifyNotDuplicate : ICommandValidator<CreateEmployee>
    {
        readonly IReadOnlyRepository<Employee, Guid> _readOnlyRepository;

        public VerifyNotDuplicate(IReadOnlyRepository<Employee, Guid> readOnlyRepository)
        {
            _readOnlyRepository = readOnlyRepository;
        }

        public Task Validate(CreateEmployee command)
        {
            var matches = _readOnlyRepository.Set()
                .Where(x =>
                    x.FirstName == command.FirstName && x.MiddleName == command.MiddleName &&
                    x.LastName == command.LastName && x.SecondLastName == command.SecondLastName);
            
            if (matches.Any())
                throw new CommandValidationException(new List<ValidationFailure>
                    {new ValidationFailure("Names", "Employee with that name already exists.")});

            return Task.CompletedTask;
        }
    }
}