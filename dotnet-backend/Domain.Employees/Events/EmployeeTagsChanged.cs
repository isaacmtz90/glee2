using System;
using Domain.Common.Entities;

namespace Domain.Employees.Events
{
    public class EmployeeTagsChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string Tags { get; }


        public EmployeeTagsChanged(Guid employeeId, string tags)
        {
            EmployeeId = employeeId;
            Tags = tags;
        }
    }
}