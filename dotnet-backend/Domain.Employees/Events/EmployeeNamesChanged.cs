using System;
using Domain.Common.Entities;

namespace Domain.Employees.Events
{
    public class EmployeeNamesChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string FirstName { get; }
        public string MiddleName { get; }
        public string LastName { get; }
        public string SecondLastName { get; }

        public EmployeeNamesChanged(Guid employeeId, string firstName, string middleName, string lastName, string secondLastName)
        {
            EmployeeId = employeeId;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            SecondLastName = secondLastName;
        }
    }
}