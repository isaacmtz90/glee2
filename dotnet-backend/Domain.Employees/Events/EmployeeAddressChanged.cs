using System;
using Domain.Common.Entities;

namespace Domain.Employees.Events
{
    public class EmployeeAddressChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string Address { get; }
        public string City { get; }
        public string Region { get; }
        public string Country { get; }


        public EmployeeAddressChanged(Guid employeeId, string address, string city, string region, string country)
        {
            EmployeeId = employeeId;
            Address = address;
            City = city;
            Region = region;
            Country = country;
        }
    }
}