using System;
using Domain.Common.Entities;

namespace Domain.Employees.Events
{
    public class EmployeePersonalEmailChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string NewPersonalEmail { get; }

        public EmployeePersonalEmailChanged(Guid employeeId, string newPersonalEmail)
        {
            EmployeeId = employeeId;
            NewPersonalEmail = newPersonalEmail;
        }
    }
}