using System;
using Domain.Common.Entities;

namespace Domain.Employees.Events
{
    public class EmployeeCompanyEmailChanged : IEvent
    {
        public Guid EmployeeId { get; }
        public string NewCompanyEmail { get; set; }
        public EmployeeCompanyEmailChanged(Guid employeeId, string newCompanyEmail)
        {
            EmployeeId = employeeId;
            NewCompanyEmail = newCompanyEmail;
        }

    }
}