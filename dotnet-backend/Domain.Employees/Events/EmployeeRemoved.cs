using System;
using Domain.Common.Entities;

namespace Domain.Employees.Events
{
    public class EmployeeRemoved : IEvent
    {
        public Guid EmployeeId { get; }

        public EmployeeRemoved(Guid employeeId)
        {
            EmployeeId = employeeId;
        }

    }
}