using System.Threading.Tasks;
using AcklenAvenue.Events;

namespace Domain.Employees.Events.Handlers
{
    public class NotifyEmployeeRemovedToConsole : IEventHandler<EmployeeRemoved>
    {
        public Task Handle(EmployeeRemoved command)
        {
            return Task.CompletedTask;
        }
    }
}