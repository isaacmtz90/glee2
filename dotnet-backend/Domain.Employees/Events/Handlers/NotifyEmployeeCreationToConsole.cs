using System;
using System.Threading.Tasks;
using AcklenAvenue.Events;

namespace Domain.Employees.Events.Handlers
{
    public class NotifyEmployeeCreationToConsole : IEventHandler<EmployeeCreated>
    {
        public Task Handle(EmployeeCreated command)
        {
            Console.WriteLine($"Employee {command.DisplayName} was created");
            return Task.CompletedTask;
        }
    }
}