using System;
using Domain.Common.Entities;

namespace Domain.Employees.Events
{
    public class EmployeePhoneNumberChanged : IEvent
    {
        public Guid Id { get; }
        public string NewPhoneNumber { get; }

        public EmployeePhoneNumberChanged(Guid id, string newPhoneNumber)
        {
            Id = id;
            NewPhoneNumber = newPhoneNumber;
        }
    }
}