# Glee Dotnet Core Backend

## CI/CD
...

## Review Apps
..

## Environment Variables

BUILD_VERSION - This env variable should be set during CI/CD in the build steps so that the version makes it into the `Web/Web.csproj` file and eventually is made available in the `/status` endpoint.

## Setup
Create your own  a copy of the environment variable files. You can do so by running `$ cp Web/develop.env Web/.env` while inside the `dotnet-backend` folder. 


You'll need to add env variables for the authentication scheme of your choise (or both).

Current authentication schemes supported: 
- HS256   (env: JWT_HS_SECRET_KEY - string)
- RS256   (env: JWT_RS_PUBLIC_KEY -  [xml](https://superdry.apphb.com/tools/online-rsa-key-converter))

You'll need to add the corresponding env variables to your `.env` file inside the `/Web` folder.

## Docker
To run the project on a docker local environment in watch mode, run `docker-compose up` inside this folder. 
To run the project on a "deployment" environment, run `docker-compose -f docker-compose-deployment.yaml up --build
` to use the alternative deployment dockerfile.

**Docker support is experimental at this moment. We may retake this once Rider is able to debug inside docker-compose.**

## Building from the Command Line

# Run Cake
The sh script below will install any needed dependencies and will attempt to build the project.

- for linux/MacOs:
```
cd dotnet-backend
./build.sh
```
- on Windows:
```
dotnet build 
```


## API docs
 The API documentation can be accessed using Swagger via <url>/swagger.
 You can add a JWT token to your requests by [generating one](http://jwtbuilder.jamiekurtz.com) and adding it to the `Authorize` section.
