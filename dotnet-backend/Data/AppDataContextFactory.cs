using System;
using dotenv.net;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Data
{
    public class AppDataContextFactory : IDesignTimeDbContextFactory<AppDataContext>
    {
        public AppDataContextFactory()
        {
        }

        public AppDataContext CreateDbContext(string[] args)
        {
            DotEnv.Config(false, "..//.//Web/.env");
            var builder = new DbContextOptionsBuilder<AppDataContext>();
            var connectionString = Environment.GetEnvironmentVariable("DB_CONNECTION");
            if (connectionString != null) builder.UseSqlServer(connectionString);

            return new AppDataContext(builder.Options);
        }
    }
}