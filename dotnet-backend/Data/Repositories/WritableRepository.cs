using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AcklenAvenue.Events;
using Domain.Common.Entities;
using Domain.Common.Repositories;
using JetBrains.Annotations;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class WritableRepository<T, TPKey> : IWritableRepository<T, TPKey> where T : class, IAggregate
    {
        readonly IReadOnlyRepository<T, TPKey> _readOnlyRepository;
        readonly IEventDispatcher _dispatcher;
        readonly AppDataContext _context;

        public WritableRepository(IReadOnlyRepository<T, TPKey> readOnlyRepository, AppDataContext context, IEventDispatcher dispatcher)
        {
            _readOnlyRepository = readOnlyRepository;
            _dispatcher = dispatcher;
            _context = context;
        }

        async Task PublishEvents(T entity)
        {
            var events = entity.GetChanges().ToList();

            foreach (var domainEvent in events) await _dispatcher.Dispatch(domainEvent);
        }

        public async Task<T> Create(T entity)
        {
            _context.Set<T>().Add(entity);
            await _context.SaveChangesAsync();

            await PublishEvents(entity);

            return entity;
        }

        public async Task<T> Update(T entity)
        {
            _context.Set<T>().Update(entity);

            await _context.SaveChangesAsync();
            await PublishEvents(entity);
            return entity;
        }

        public async Task<T> Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
            await _context.SaveChangesAsync();
            await PublishEvents(entity);
            return entity;
        }

        public IQueryable<T> Set()
        {
            return _readOnlyRepository.Set();
        }

        public Task<T> GetById(TPKey id)
        {
            return _readOnlyRepository.GetById(id);
        }

        public IQueryable<T> Find(IDictionary<string, string> parameters)
        {
            return _readOnlyRepository.Find(parameters);
        }
    }
}