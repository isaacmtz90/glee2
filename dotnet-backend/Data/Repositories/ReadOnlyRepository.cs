using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Domain.Common.Entities;
using Domain.Common.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Data.Repositories
{
    public class ReadOnlyRepository<T, TPKey> : IReadOnlyRepository<T, TPKey> where T : class, IAggregate
    {
        public ReadOnlyRepository(AppDataContext context)
        {
            Context = context;
        }

        AppDataContext Context { get; }

        public async Task<IEnumerable<T>> FindAll()
        {
            var list = await Context.Set<T>()
                .ToListAsync();

            var notRemoved = list.Where(x=> !x.Removed);
            
            return notRemoved;
        }

        public IQueryable<T> Set()
        {
            return Context.Set<T>();
        }

        public async Task<T> GetById(TPKey id)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));
            
            var entity = await Context.Set<T>().FindAsync(id);
            if (entity == null) throw new EntityNotFoundException<T>(id);
            Context.Entry(entity).State = EntityState.Detached;
            return entity;
        }

        public IQueryable<T> Find(IDictionary<string, string> parameters)
        {
            var wheres = parameters.Select(BuildWhere);
            var set = Context.Set<T>().AsQueryable();
            foreach (var @where in wheres)
            {
                set = set.Where(@where);
            }
            return set;
        }

        static Expression<Func<T, bool>> BuildWhere(KeyValuePair<string, string> parm)
        {
            var item = Expression.Parameter(typeof(T), "item");
            var prop = Expression.Property(item, parm.Key);
            var value = Expression.Constant(parm.Value);
            var equal = Expression.Equal(prop, value);
            var lambda = Expression.Lambda<Func<T, bool>>(equal, item);
            return lambda;
        }

        public async Task<IEnumerable<T>> FindByCondition(Expression<Func<T, bool>> expression,
            bool allowRemoved = false)
        {
            var list = Context.Set<T>()
                .Where(expression);

            if (!allowRemoved)
                list = list.Where(x => !x.Removed);

            return await list.ToListAsync();
        }
    }
}