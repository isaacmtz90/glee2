using System;

namespace Data.Repositories
{
    public class NotFoundException<T> : Exception
    {
        public NotFoundException(object id) : base($"{typeof(T).Name} not found for id {id.ToString()}.")
        {
        }
    }
}