using System;

namespace Data.Repositories
{
    public class EntityNotFoundException<T> : Exception
    {
        public EntityNotFoundException(params object[] id) : base($"{typeof(T).Name} not found for id {string.Join(", ", id)}.")
        {
        }
    }
}