﻿using System;
using Domain.Common.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Internal;
using Pluralize.NET.Core;

namespace Data
{
    public class AppDataContext : DbContext
    {
        public AppDataContext(DbContextOptions<AppDataContext> options) : base(options)
        {        
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var scanner = new TypeScanner.TypeScanner();
            var entityTypes = scanner.GetTypesOf<Aggregate<Guid>>();
            foreach (var entity in entityTypes)
            {
                var entityTypeBuilder = modelBuilder.Entity(entity);
                entityTypeBuilder.ToTable(new Pluralizer().Pluralize(entity.Name));
            }

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning));
            base.OnConfiguring(optionsBuilder);
        }
    }
}