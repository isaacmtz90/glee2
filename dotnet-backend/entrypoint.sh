#!/usr/bin/env bash
set -e
run_cmd="dotnet watch --project Web run --urls http://0.0.0.0:5000"

until dotnet ef database update --msbuildprojectextensionspath Data/obj/container --project Data; do
>&2 echo "SQL Server is starting up"
sleep 1
done

>&2 echo "SQL Server is up - executing command"
exec $run_cmd