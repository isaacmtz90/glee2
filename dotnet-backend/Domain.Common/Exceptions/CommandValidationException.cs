using System;
using System.Collections.Generic;
using FluentValidation.Results;

namespace Domain.Common.Exceptions
{
    public class CommandValidationException : Exception
    {
        public IList<ValidationFailure> ValidationFailures { get; }
        public CommandValidationException(IList<ValidationFailure> validationFailures)
        {
            ValidationFailures = validationFailures;
        }
    }
}