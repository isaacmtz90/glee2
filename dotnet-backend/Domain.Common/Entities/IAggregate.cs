using System.Collections.Generic;

namespace Domain.Common.Entities
{
    public interface IAggregate
    {
        bool Removed { get; }

        IEnumerable<IEvent> GetChanges();
    }
    public interface IAggregate<out TPKey> :IAggregate
    {
        TPKey Id { get; }
    }
}