using System.Collections.Generic;
using System.Linq;

namespace Domain.Common.Entities
{
    public abstract class Aggregate<TPKey> : IAggregate<TPKey>
    {
        List<IEvent> DomainEvents { get; } = new List<IEvent>();

        public abstract TPKey Id { get; protected set; }

        protected T NewChange<T>(T domainEvent) where T : IEvent
        {
            DomainEvents.Add(domainEvent);
            return domainEvent;
        }

        public virtual bool Removed { get; protected set; }

        public IEnumerable<IEvent> GetChanges()
        {
            var publishEvents = DomainEvents.ToList();
            DomainEvents.Clear();
            return publishEvents;
        }
    }
}