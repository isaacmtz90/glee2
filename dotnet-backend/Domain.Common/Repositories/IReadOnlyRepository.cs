using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Common.Repositories
{
    public interface IReadOnlyRepository
    {
        
    }
    public interface IReadOnlyRepository<T, in TPKey> : IReadOnlyRepository
    {
        IQueryable<T> Set();
        Task<T> GetById(TPKey id);
        IQueryable<T> Find(IDictionary<string,string> parameters);
    }
}