using System.Threading.Tasks;

namespace Domain.Common.Repositories
{
    public interface IWritableRepository
    {
        
    }
    
    public interface IWritableRepository<T, in TPKey> : IWritableRepository, IReadOnlyRepository<T, TPKey>
    {
        Task<T> Create(T entity);
        Task<T> Update(T entity);

        Task<T> Delete(T entity);
    }
}