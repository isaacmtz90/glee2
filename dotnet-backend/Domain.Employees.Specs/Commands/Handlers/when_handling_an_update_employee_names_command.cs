using System;
using System.Threading.Tasks;
using Domain.Common.Repositories;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Handlers;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Handlers
{
    [Subject(typeof(EmployeeNamesUpdater))]
    public class when_handling_an_change_employee_names_command
    {
        static IWritableRepository<Employee, Guid> _writableRepository;
        static EmployeeNamesUpdater _handler;
        static ChangeEmployeeNames _changeEmployeeNames;
        static Employee _employee;

        Establish context = () =>
        {
            _writableRepository = Mock.Of<IWritableRepository<Employee, Guid>>();


            _employee = Mock.Of<Employee>();

            _newSecondLastName = "new second last name";
            _newLastName = "new last name";
            _newMiddleName = "new middle name";
            _newFirstName = "new first name";

            _changeEmployeeNames = Builder<ChangeEmployeeNames>.CreateNew()
                .With(x => x.Id, _employee.Id)
                .With(x => x.FirstName, _newFirstName)
                .With(x => x.MiddleName, _newMiddleName)
                .With(x => x.LastName, _newLastName)
                .With(x => x.SecondLastName, _newSecondLastName)
                .Build();

            Mock.Get(_writableRepository).Setup(x => x.GetById(_employee.Id))
                .Returns(Task.FromResult(_employee));

            _handler = new EmployeeNamesUpdater(_writableRepository);
        };

        Because of = () => _handler.Handle(_changeEmployeeNames).Await();

        It should_update_the_employee_in_the_db = () =>
        {
            Mock.Get(_writableRepository).Verify(repository =>
                repository.Update(Moq.It.Is<Employee>(employee => _employee.ShouldBeEquivalent(employee))));
        };

        It should_tell_the_employee_to_change_names = () =>
        {
            Mock.Get(_employee).Verify(x =>
                x.ChangeNames(_newFirstName, _newMiddleName, _newLastName, _newSecondLastName));
        };

        static string _newSecondLastName;
        static string _newLastName;
        static string _newMiddleName;
        static string _newFirstName;
    }
}