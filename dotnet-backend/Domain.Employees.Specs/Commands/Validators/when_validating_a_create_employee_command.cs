using System;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Validators.ForCreateEmployee;
using FizzWare.NBuilder;
using Machine.Specifications;
using Testing.Utilities;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Validators
{
    public class when_validating_a_create_employee_command
    {
        static ValidateCreateEmployee _validator;

        Establish context = () =>
        {
            _validator = new ValidateCreateEmployee();
            _command = Builder<CreateEmployee>.CreateNew()
                .With(employee => employee.FirstName, "")
                .With(e => e.LastName, "")
                .With(e => e.CompanyEmail, "")
                .Build();
        };

        Because of = () => _result = Catch.Exception(() => _validator.Validate(_command).Await());

        It should_return_errors_for_first_name = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "FirstName");

        It should_return_errors_for_last_name = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "LastName");

        It should_return_errors_for_email = () =>
            _result.ShouldIncludeValidationFailureLike(x => x.PropertyName == "CompanyEmail");

        static CreateEmployee _command;
        static Exception _result;
    }
}