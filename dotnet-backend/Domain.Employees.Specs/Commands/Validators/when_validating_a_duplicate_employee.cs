using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Common.Repositories;
using Domain.Employees.Commands;
using Domain.Employees.Commands.Validators.ForCreateEmployee;
using FizzWare.NBuilder;
using Machine.Specifications;
using Moq;
using Testing.Utilities;
using It = Machine.Specifications.It;

namespace Domain.Employees.Specs.Commands.Validators
{
    public class when_validating_a_duplicate_employee
    {
        Establish _context = () =>
        {
            _readOnlyRepository = Mock.Of<IReadOnlyRepository<Employee, Guid>>();
            _systemUnderTest = new VerifyNotDuplicate(_readOnlyRepository);
            _command = Builder<CreateEmployee>.CreateNew().Build();
            
            _matchingEmployee = Builder<Employee>.CreateNew()
                .With(x => x.FirstName, _command.FirstName)
                .With(x => x.MiddleName, _command.MiddleName)
                .With(x => x.LastName, _command.LastName)
                .With(x => x.SecondLastName, _command.SecondLastName)
                .Build();

            Mock.Get(_readOnlyRepository).Setup(x => x.Set())
                .Returns(new List<Employee> {_matchingEmployee}.AsQueryable());
        };

        Because of = () => _result = Catch.Exception(() => _systemUnderTest.Validate(_command).Await());

        It should_return_errors_for_first_name = () =>
            _result.ShouldIncludeValidationFailureLike(x =>
                x.PropertyName == "Names" && x.ErrorMessage == "Employee with that name already exists.");

        static CreateEmployee _command;
        static IReadOnlyRepository<Employee, Guid> _readOnlyRepository;
        static Employee _matchingEmployee;
        static VerifyNotDuplicate _systemUnderTest;
        static Exception _result;
    }
}