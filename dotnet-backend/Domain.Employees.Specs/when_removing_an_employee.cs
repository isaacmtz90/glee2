using FizzWare.NBuilder;
using Machine.Specifications;

namespace Domain.Employees.Specs
{
    public class when_removing_an_employee
    {
        Establish _context = () =>
        {
            _systemUnderTest = Builder<Employee>.CreateNew()
                .With(x => x.Removed, false)
                .Build();
        };

        Because of = () => { _systemUnderTest.Remove(); };

        It should_set_the_employee_as_removed = () => { _systemUnderTest.Removed.ShouldBeTrue(); };
        
        static Employee _systemUnderTest;
    }
}