FROM microsoft/dotnet:2.2-sdk  AS builder
WORKDIR /glee2

# Copy the source project files
COPY . ./

RUN ["dotnet", "restore"] 
RUN ["dotnet", "build"] 
EXPOSE 5000
ENTRYPOINT ["dotnet", "run", "-p", "Web", "--urls", "http://0.0.0.0:5000"]