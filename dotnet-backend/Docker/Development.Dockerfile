FROM mcr.microsoft.com/dotnet/core/sdk:2.2 as build
ENV DOTNET_USE_POLLING_FILE_WATCHER 1
WORKDIR /app
COPY ./entrypoint.sh ./entrypoint.sh
RUN chmod +x ./entrypoint.sh
CMD /bin/bash ./entrypoint.sh