using System;
using System.Threading.Tasks;
using Data;
using Microsoft.AspNetCore.Http;

namespace Web.Infrastructure
{
    [Boilerplate]
    public class UnitOfWork
    {
        private readonly RequestDelegate _next;

        public UnitOfWork(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, AppDataContext dbContext)
        {
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    await _next(context);

                    await dbContext.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();

                    throw;
                }
            }
        }
    }
}