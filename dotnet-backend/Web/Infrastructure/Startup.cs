﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Web.Infrastructure.Bootstrapping;
using Web.Infrastructure.ExceptionHandling;

namespace Web.Infrastructure
{
    [Boilerplate]
    public class Startup
    {
        IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            var isDevelopment = Configuration["ENVIRONMENT"] == "development";

            services
                .ConfigureEnvironmentVariables()
                .ConfigureDatabaseContext(Configuration["DB_CONNECTION"], isDevelopment)
                .ConfigureCommands()
                .ConfigureAuthentication(
                    Configuration["JWT_ISSUER"],
                    Configuration["JWT_HS_SECRET_KEY"],
                    Configuration["JWT_RS_PUBLIC_KEY"])
                .ConfigureEvents()
                .ConfigureDataDependencies()
                .ConfigureWebDependencies()
                .ConfigureAutoMapper()
                .ConfigureAuxiliaryServices();

            if (isDevelopment) services.ConfigureSwagger();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (Configuration["ENVIRONMENT"] == "development")
                app
                    .SetupAppForDevelopmentMode()
                    .UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app
                .UseAuthentication()
                .UseHttpsRedirection()
                .UseMvc()
                .UseMiddleware<UnitOfWork>()
                .UseMiddleware<ExceptionHandlerMiddleware>()
                .MigrateDatabase();
        }
    }
}