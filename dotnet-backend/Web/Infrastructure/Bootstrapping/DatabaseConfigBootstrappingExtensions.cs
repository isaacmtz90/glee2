using Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Infrastructure.Bootstrapping
{
    public static class DatabaseConfigBootstrappingExtensions
    {
        public static IServiceCollection ConfigureDatabaseContext(this IServiceCollection services,
            string connectionString, bool isDevelopment = false)
        {
            services.AddDbContext<AppDataContext>(options =>
            {
                if (isDevelopment || !string.IsNullOrEmpty(connectionString))
                    options.UseSqlite("Data Source=testing.db");
                else
                    options.UseSqlServer(connectionString);

                options.EnableSensitiveDataLogging();
            });
            return services;
        }

        public static IApplicationBuilder MigrateDatabase(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<AppDataContext>();
                context.Database.Migrate();
            }

            return app;
        }
    }
}