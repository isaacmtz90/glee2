using AutoMapper;
using Domain.Employees;
using Microsoft.Extensions.DependencyInjection;
using Web.Employees.Responses;

namespace Web.Infrastructure.Bootstrapping
{
    public static class AutoMapperBootstrapper
    {
        public static IServiceCollection ConfigureAutoMapper(this IServiceCollection services)
        {
            var config = new MapperConfiguration(cfg => { cfg.CreateMap<Employee, EmployeeResponse>(); });

            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);
            return services;
        }
    }
}