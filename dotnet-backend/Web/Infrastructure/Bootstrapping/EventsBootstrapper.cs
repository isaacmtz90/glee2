using AcklenAvenue.Events;
using Domain.Employees;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Infrastructure.Bootstrapping
{
    public static class EventsBootstrapper
    {
        public static IServiceCollection ConfigureEvents(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<Employee>()
                    .AddClasses(c => c.AssignableTo(typeof(IEventHandler)))
                    .AsImplementedInterfaces().WithTransientLifetime()
            );

            services.AddTransient<IEventDispatcher, WorkerEventDispatcher>();

            return services;
        }

    }
}