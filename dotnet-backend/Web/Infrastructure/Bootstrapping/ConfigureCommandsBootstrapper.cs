using AcklenAvenue.Commands;
using Domain.Employees;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Infrastructure.Bootstrapping
{
    public static class ConfigureCommandsBootstrapper
    {
        public static IServiceCollection ConfigureCommands(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<Employee>()
                    .AddClasses(c => c.AssignableTo<ICommandHandler>())
                    .AsSelfWithInterfaces().WithTransientLifetime()
                    .AddClasses(c => c.AssignableTo<ICommandValidator>())
                    .AsSelfWithInterfaces().WithTransientLifetime()
            );


            services.AddTransient<ICommandDispatcher, WorkerCommandDispatcher>();
            services.Decorate<ICommandDispatcher, ValidatedCommandDispatcher>();
            return services;
        }

    }
}