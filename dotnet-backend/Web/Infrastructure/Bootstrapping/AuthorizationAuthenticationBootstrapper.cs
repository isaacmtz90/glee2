using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Web.Infrastructure.ExceptionHandling;
using Web.Infrastructure.ExceptionHandling.Exceptions;
using Web.Infrastructure.Helpers;

namespace Web.Infrastructure.Bootstrapping
{
    public static class AuthorizationAuthenticationBootstrapper
    {
        const string Rs256Scheme = "RS256";
        const string Hs256Scheme = "HS256";

        public static IServiceCollection ConfigureAuthentication(this IServiceCollection services, string issuer,
            string hsSecretKey, string rsPublicKey)
        {
            var authenticationBuilder = services.AddAuthentication();
            var activeSchemes = new List<string>
            {
                AttemptToAddRs256(issuer, rsPublicKey, authenticationBuilder),
                AttemptToAddHs256(issuer, hsSecretKey, authenticationBuilder)
            }.Where(x => !string.IsNullOrEmpty(x)).ToList();

            if (!activeSchemes.Any())
            {
                throw new Exception("No active authentication schemes available. API would be exposed. Please include keys and issuer for HS256, RS256 or both in the environment config.");
            }

            services.AddAuthorization(options =>
            {
                var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(activeSchemes.ToArray());
                defaultAuthorizationPolicyBuilder = defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();
                options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
            });
            return services;
        }

        static string AttemptToAddHs256(string issuer, string hsSecretKey, AuthenticationBuilder authenticationBuilder)
        {
            if (string.IsNullOrEmpty(issuer) || string.IsNullOrEmpty(hsSecretKey)) return "";
            
            var bearerParametersForHs256 = GetBearerParametersForHs256(hsSecretKey, issuer);

            authenticationBuilder
                .AddJwtBearer(Hs256Scheme, options => options.TokenValidationParameters = bearerParametersForHs256);

            return Hs256Scheme;
        }

        static string AttemptToAddRs256(string issuer, string rsPublicKey, AuthenticationBuilder authenticationBuilder)
        {
            if (string.IsNullOrEmpty(issuer) || string.IsNullOrEmpty(rsPublicKey)) return "";
            
            var bearerParametersForRs256 = GetBearerParametersForRs256(rsPublicKey, issuer);

            authenticationBuilder
                .AddJwtBearer(Rs256Scheme, options => options.TokenValidationParameters = bearerParametersForRs256);

            return Rs256Scheme;
        }

        static TokenValidationParameters GetBearerParametersForRs256(string rsPublicKey, string issuer)
        {
            SecurityKey issuerSigningKey;

            using (var publicRsa = RSA.Create())
            {
                try
                {
                    publicRsa.CreateRsaKeyFromXmlString(rsPublicKey);
                    issuerSigningKey = new RsaSecurityKey(publicRsa);
                }
                catch (Exception ex)
                {
                    throw new InvalidConfigurationException(
                        $"RS Public Key must be valid XML.", ex);
                }
            }

            return BuildBearerParameters(issuerSigningKey, issuer);
        }

        static TokenValidationParameters GetBearerParametersForHs256(string hsSecretKey, string issuer)
        {
            try
            {
                SecurityKey issuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(hsSecretKey));

                return BuildBearerParameters(issuerSigningKey, issuer);
            }
            catch (Exception ex)
            {
                throw new InvalidConfigurationException($"Invalid HS Secret Key environment variable.", ex);
            }
        }

        static TokenValidationParameters BuildBearerParameters(SecurityKey issuerSigningKey, string issuer)
        {
            return new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidIssuer = issuer,
                IssuerSigningKey = issuerSigningKey
            };
        }
    }
}