namespace Web.Infrastructure.Bootstrapping
{
    [Boilerplate]
    public class SwaggerConfiguration
    {

        /// <summary>
        /// <para>Glee2-dotnet API v1</para>
        /// </summary>
        public const string EndpointDescription = "Glee2-dotnet API v1";

        /// <summary>
        /// <para>/swagger/v1/swagger.json</para>
        /// </summary>
        public const string EndpointUrl = "/swagger/v1/swagger.json";

        /// <summary>
        /// <para>Acklen Avenue</para>
        /// </summary>
        public const string ContactName = "Acklen Avenue";

        /// <summary>
        /// <para>https://acklenavenue.com/</para>
        /// </summary>
        public const string ContactUrl = "https://acklenavenue.com/";

        /// <summary>
        /// <para>v1</para>
        /// </summary>
        public const string DocNameV1 = "v1";

        /// <summary>
        /// <para>Foo API</para>
        /// </summary>
        public const string DocInfoTitle = "Glee2-dotnet API";

        /// <summary>
        /// <para>v1</para>
        /// </summary>
        public const string DocInfoVersion = "v1";

        /// <summary>
        /// <para>Foo Api - Sample Web API in ASP.NET Core 2</para>
        /// </summary>
        public const string DocInfoDescription = "Glee2-dotnet Api - Sample Web API in ASP.NET Core 2";

    }
}