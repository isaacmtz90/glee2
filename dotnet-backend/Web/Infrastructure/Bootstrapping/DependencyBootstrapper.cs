using System;
using Data.Repositories;
using Domain.Common.Repositories;
using Domain.Common.Services;
using Microsoft.Extensions.DependencyInjection;
using Services;
using Web.Infrastructure.ExceptionHandling;
using Web.Infrastructure.ExceptionHandling.Renderers;
using Web.Status;

namespace Web.Infrastructure.Bootstrapping
{
    public static class DependencyBootstrapper
    {
        public static IServiceCollection ConfigureDataDependencies(this IServiceCollection services)
        {
            services.AddScoped(typeof(IReadOnlyRepository<,>), typeof(ReadOnlyRepository<,>));
            services.AddScoped(typeof(IWritableRepository<,>), typeof(WritableRepository<,>));
            return services;
        }

        public static IServiceCollection ConfigureWebDependencies(this IServiceCollection services)
        {
            services.Scan(scan =>
                scan.FromAssemblyOf<IExceptionRender>()
                    .AddClasses(c => c.AssignableTo<IExceptionRender>())
                    .AsImplementedInterfaces().WithSingletonLifetime()
            );
            return services;
        }

        public static IServiceCollection ConfigureAuxiliaryServices(this IServiceCollection services)
        {
            services.AddSingleton<IIdentityGenerator<Guid>, GuidIdentityGenerator>();
            services.AddSingleton<IApiInformationGetter, DefaultApiInformationGetter>();
            return services;
        }
    }
}