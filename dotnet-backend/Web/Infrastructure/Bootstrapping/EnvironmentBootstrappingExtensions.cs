using System.Text;
using dotenv.net.DependencyInjection.Extensions;
using Microsoft.Extensions.DependencyInjection;

namespace Web.Infrastructure.Bootstrapping
{
    public static class EnvironmentBootstrappingExtensions
    {
        public static IServiceCollection ConfigureEnvironmentVariables(this IServiceCollection services)
        {
            services.AddEnv(builder =>
            {
                builder
                    .AddEnvFile(".env")
                    .AddThrowOnError(false)
                    .AddEncoding(Encoding.ASCII);
            });
            return services;
        }
    }
}