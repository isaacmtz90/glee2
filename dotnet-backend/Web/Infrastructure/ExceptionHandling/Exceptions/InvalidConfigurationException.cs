using System;

namespace Web.Infrastructure.ExceptionHandling.Exceptions
{
    public class InvalidConfigurationException : Exception
    {
        public InvalidConfigurationException(string message, Exception innerException): base(message, innerException)
        {
            
        }
    }
}