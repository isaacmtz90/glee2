using System;

namespace Web.Employees.Responses
{
    public class EmployeeResponse
    {
        public Guid Id { get; set; } = Guid.Empty;
        public string FirstName { get; set; } = "";
        public string? MiddleName { get; set; }
        public string LastName { get; set; } = "";
        public string SecondLastName { get; set; } = "";
        public string DisplayName { get; set; } = "";
        public string Email { get; set; } = "";
        public string PersonalEmail { get; set; } = "";
        public string Birthdate { get; set; } = "";
        public string StartDate { get; set; } = "";
        public string Address { get; set; } = "";
        public string PhoneNumber { get; set; } = "";
        public string BankName { get; set; } = "";
        public string AccountNumber { get; set; } = "";
        public string Gender { get; set; } = "";
        public string Tags { get; set; } = "";
        public string Country { get; set; } = "";
        public string Region { get; set; } = "";
        public string City { get; set; } = "";
        public string Salary { get; set; } = "";
        public string EffectiveDate { get; set; } = "";
        public string SalaryType { get; set; } = "";
    }
}