using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AcklenAvenue.Commands;
using AutoMapper;
using Domain.Common.Repositories;
using Domain.Common.Services;
using Domain.Employees;
using Domain.Employees.Commands;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Web.Employees.Requests;
using Web.Employees.Responses;

namespace Web.Employees
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmployeesController
    {
        readonly ICommandDispatcher _commandDispatcher;
        readonly IMapper _mapper;
        readonly IIdentityGenerator<Guid> _identityGenerator;
        readonly IReadOnlyRepository<Employee, Guid> _readOnlyRepository;

        public EmployeesController(ICommandDispatcher commandDispatcher,
            IReadOnlyRepository<Employee, Guid> readOnlyRepository,
            IMapper mapper, IIdentityGenerator<Guid> identityGenerator)
        {
            _commandDispatcher = commandDispatcher;
            _readOnlyRepository = readOnlyRepository;
            _mapper = mapper;
            _identityGenerator = identityGenerator;
        }

        [HttpPost]
        public async Task<NewEmployeeResponse> CreateEmployee(
            [FromBody] CreateEmployeeRequest createCreateEmployeeRequest)
        {
            var id = _identityGenerator.Generate();

            await _commandDispatcher.Dispatch(new CreateEmployee(id, createCreateEmployeeRequest.FirstName,
                createCreateEmployeeRequest.DisplayName, createCreateEmployeeRequest.LastName,
                createCreateEmployeeRequest.Gender,
                createCreateEmployeeRequest.Salary, createCreateEmployeeRequest.MiddleName,
                createCreateEmployeeRequest.SecondLastName,
                createCreateEmployeeRequest.Email, createCreateEmployeeRequest.PersonalEmail,
                createCreateEmployeeRequest.Birthdate,
                createCreateEmployeeRequest.StartDate, createCreateEmployeeRequest.Address,
                createCreateEmployeeRequest.PhoneNumber,
                createCreateEmployeeRequest.BankName, createCreateEmployeeRequest.AccountNumber,
                createCreateEmployeeRequest.Tags,
                createCreateEmployeeRequest.Country, createCreateEmployeeRequest.Region,
                createCreateEmployeeRequest.City,
                createCreateEmployeeRequest.EffectiveDate, createCreateEmployeeRequest.SalaryType));

            return new NewEmployeeResponse(id);
        }

        int GetValueOrDefault(IDictionary<string, string> parameters, string key, int @default)
        {
            var item = parameters.Where(x => string.Equals(x.Key, key, StringComparison.CurrentCultureIgnoreCase))
                .ToList();
            return !item.Any() ? @default : Convert.ToInt32(item.First().Value);
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<EmployeeResponse> GetOne([FromRoute] Guid id)
        {
            var employee = await _readOnlyRepository.GetById(id);
            var response = _mapper.Map<Employee, EmployeeResponse>(employee);
            return response;
        }

        [HttpGet]
        public async Task<IEnumerable<EmployeeResponse>> GetAll([FromQuery] IDictionary<string, string> parameters)
        {
            var pageSize = GetValueOrDefault(parameters, "pageSize", 10);
            var pageNumber = GetValueOrDefault(parameters, "pageNumber", 1);

            var searchParams = parameters.Where(x =>
                    !string.Equals(x.Key, "pageSize", StringComparison.CurrentCultureIgnoreCase) &&
                    !string.Equals(x.Key, "pageNumber", StringComparison.CurrentCultureIgnoreCase))
                .ToDictionary(x => x.Key, x => x.Value);

            var employees = parameters.Any()
                ? _readOnlyRepository.Find(searchParams)
                : _readOnlyRepository.Set();

            var paged = await employees.Skip(pageSize * (pageNumber - 1))
                .Take(pageSize).ToListAsync();

            var result = _mapper.Map<IEnumerable<Employee>, IEnumerable<EmployeeResponse>>(
                paged);

            return result;
        }

        [HttpPut]
        [Route("{id}/names")]
        public async Task ChangeNames([FromRoute] Guid id, [FromBody] UpdateEmployeeRequest request)
        {
            var command = new ChangeEmployeeNames(id,
                request.FirstName, request.MiddleName, request.LastName, request.SecondLastName);
            
            await _commandDispatcher.Dispatch(command);
        }

        [HttpPut]
        [Route("{id}/address")]
        public async Task ChangeAddress([FromRoute] Guid id, [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeeAddress(id,
                updateEmployeeRequest.Address, updateEmployeeRequest.City, updateEmployeeRequest.Region,
                updateEmployeeRequest.Country));
        }

        [HttpPut]
        [Route("{id}/displayName")]
        public async Task ChangeDisplayName([FromRoute] Guid id, [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeeDisplayName(id,
                updateEmployeeRequest.DisplayName));
        }

        [HttpPut]
        [Route("{id}/tags")]
        public async Task ChangeTags([FromRoute] Guid id, [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeeTags(id,
                updateEmployeeRequest.Tags));
        }

        [HttpPut]
        [Route("{id}/phoneNumber")]
        public async Task ChangePhone([FromRoute] Guid id, [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeePhoneNumber(id,
                updateEmployeeRequest.PhoneNumber));
        }

        [HttpPut]
        [Route("{id}/personalEmail")]
        public async Task ChangePersonalEmail([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeePersonalEmail(id,
                updateEmployeeRequest.PersonalEmail));
        }

        [HttpPut]
        [Route("{id}/companyEmail")]
        public async Task ChangeCompanyEmail([FromRoute] Guid id,
            [FromBody] UpdateEmployeeRequest updateEmployeeRequest)
        {
            await _commandDispatcher.Dispatch(new ChangeEmployeeCompanyEmail(id,
                updateEmployeeRequest.CompanyEmail));
        }

        [HttpDelete("{employeeId}")]
        public async Task DeleteEmployee(Guid employeeId)
        {
            var removeEmployee = new RemoveEmployee(employeeId);
            await _commandDispatcher.Dispatch(removeEmployee);
        }
    }
}