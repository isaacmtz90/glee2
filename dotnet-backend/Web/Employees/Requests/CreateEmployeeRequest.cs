using System;

namespace Web.Employees.Requests
{
    public class CreateEmployeeRequest
    {
        public string FirstName { get; set; } = "";
        public string? MiddleName { get; set; } 
        public string LastName { get; set; } = "";
        public string? SecondLastName { get; set; }
        public string DisplayName { get; set; } = "";
        public string Email { get; set; } = "";
        public string PersonalEmail { get; set; } = "";
        public DateTime Birthdate { get; set; }=DateTime.MinValue;
        public DateTime StartDate { get; set; }=DateTime.MinValue;
        public string Address { get; set; } = "";
        public string PhoneNumber { get; set; } = "";
        public string BankName { get; set; } = "";
        public string AccountNumber { get; set; } = "";
        public string Gender { get; set; } = "";
        public string Tags { get; set; } = "";
        public string Country { get; set; } = "";
        public string Region { get; set; } = "";
        public string City { get; set; } = "";
        public double Salary { get; set; } = 0;
        public DateTime EffectiveDate { get; set; }=DateTime.MinValue;
        public string SalaryType { get; set; } = "";
    }
}