using System.Diagnostics;
using System.Reflection;
using Web.Infrastructure;

namespace Web.Status
{
    public class DefaultApiInformationGetter : IApiInformationGetter
    {
        public ApiInformation GetInfo()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            var fileVersion = fileVersionInfo.FileVersion;
            
            return new ApiInformation(fileVersion);
        }
    }
}