using Microsoft.AspNetCore.Mvc;
using Web.Infrastructure;

namespace Web.Status
{
    [Route("api/status")]
    [ApiController]
    public class StatusController
    {
        readonly IApiInformationGetter _apiInformationGetter;

        public StatusController(IApiInformationGetter apiInformationGetter)
        {
            _apiInformationGetter = apiInformationGetter;
        }

        [HttpGet]
        public ApiInformation GetStatus() => _apiInformationGetter.GetInfo();

        [HttpGet]
        [Route("something")]
        public string GetSomething() => "something";

        [HttpGet]
        [Route("monkey/{id}")]
        public int SeaMonkey(int id)
        {
            
            return id;
        }
    }
}