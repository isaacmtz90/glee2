using System;
using System.Collections.Generic;
using System.Linq;
using Data.Repositories;
using Domain.Employees;
using FluentAssertions;
using Machine.Specifications;

namespace Data.Specs.Integration.Repositories.ReadOnly
{
    public class when_getting_projects_from_the_read_only_repository
    {
        static ReadOnlyRepository<Employee, Guid> _systemUnderTest;
        static IEnumerable<Employee> _result;
        static IEnumerable<Employee> _nonRemovedEmployees;
        static AppDataContext _appDataContext;

        Cleanup after = () => { _appDataContext.Clean(); };

        Establish context = () =>
        {
            _appDataContext = InMemoryDataContext
                .GetInMemoryContext()
                .Seed();

            _nonRemovedEmployees = _appDataContext.Set<Employee>().Where(x => !x.Removed);
            _systemUnderTest = new ReadOnlyRepository<Employee, Guid>(_appDataContext);
        };

        Because of = async () => { _result = await _systemUnderTest.FindAll(); };

        It should_return_a_list_of_non_removed_projects = () =>
            _result.Should().BeEquivalentTo(_nonRemovedEmployees);
    }
}