using System.Threading.Tasks;
using Domain.Employees;
using FizzWare.NBuilder;
using Microsoft.EntityFrameworkCore;

namespace Data.Specs.Integration.Repositories
{
    public static class InMemoryDataContext
    {
        public static AppDataContext GetInMemoryContext()
        {
            var options = new DbContextOptionsBuilder<AppDataContext>()
                .UseInMemoryDatabase("test")
                .Options;

            var appDataContext = new AppDataContext(options);

            return appDataContext;
        }

        public static AppDataContext Seed(this AppDataContext appDataContext)
        {
            var set = appDataContext.Set<Employee>();
            var projects = Builder<Employee>.CreateListOfSize(10)
                .Build();
            set.AddRange(projects);
            appDataContext.SaveChanges();
            return appDataContext;
        }

        public static void Clean(this AppDataContext appDataContext)
        {
            appDataContext.Database.EnsureDeleted();
        }
    }
}