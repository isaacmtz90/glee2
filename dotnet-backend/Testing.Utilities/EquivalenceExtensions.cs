using FluentAssertions;

namespace Testing.Utilities
{
    public static class EquivalenceExtensions
    {
        public static bool ShouldBeEquivalent(this object original, object next)
        {
            original.Should().BeEquivalentTo(next);
            return true;
        }
        
    }
}