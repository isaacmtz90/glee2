using System;
using System.Linq.Expressions;
using FluentAssertions;

namespace Testing.Utilities
{
    public class Some<T>
    {
        public static object WithCharacteristics(Expression<Func<T, bool>> func)
        {
            object withCharacteristics = Moq.It.Is<T>(func); 
            return withCharacteristics ?? new object();
        }
    
        public static object WithSamePropertyValuesAs(object comparisonObject)
        {
            object withSamePropertyValuesAs = Moq.It.Is<T>(x=> x != null && x.ShouldBeEquivalent(comparisonObject));
            return withSamePropertyValuesAs ?? new object();
        }
    }
}