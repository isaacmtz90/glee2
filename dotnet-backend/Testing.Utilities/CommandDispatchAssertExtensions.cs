using System;
using System.Linq.Expressions;
using AcklenAvenue.Commands;
using Moq;

namespace Testing.Utilities
{
    public static class CommandDispatchAssertExtensions
    {
        public static void ShouldDispatchACommandLike<T>(this Mock<ICommandDispatcher> mock, T comparisonObject)
        {
            if(comparisonObject==null) throw new ArgumentNullException(nameof(comparisonObject));
            
            mock.Verify(x=> x.Dispatch(Some<T>.WithSamePropertyValuesAs(comparisonObject)));
        }
        
        public static void ShouldDispatchACommandLike<T>(this Mock<ICommandDispatcher> mock, Expression<Func<T, bool>> func)
        {
            mock.Verify(x=> x.Dispatch(Some<T>.WithCharacteristics(func)));
        }
    }
}