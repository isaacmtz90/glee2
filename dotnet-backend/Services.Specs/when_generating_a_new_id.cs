using System;
using Domain.Common.Services;
using FluentAssertions;
using Machine.Specifications;

namespace Services.Specs
{
    public class when_generating_a_new_id
    {
        static IIdentityGenerator<Guid> _systemUnderTest;
        static Guid _result;

        Establish _context = () => { _systemUnderTest = new GuidIdentityGenerator(); };

        Because of = () => { _result = _systemUnderTest.Generate(); };

        It should_return_a_non_zero_guid = () => { _result.Should().NotBe(Guid.Empty); };
    }
}