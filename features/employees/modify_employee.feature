Feature: Modify an Employee

  In order to ___ as a ______ I can ______.

Scenario: Modify Names
Given an active employee
And the employee's name is "Jeff Alln Johnson William"
When I modify his name to "Jeffry Allan Johnsen Williams"
Then the employee's "first" name should be "Jeffry"
And the employee's "middle" name should be "Allan"
And the employee's "last" name should be "Johnsen"
And the employee's "second last" name should be "Williams"

Scenario: Cannot remove the last name
Given an active employee
And the employee's name is "Jeff Alln Johnson William"
When I attempt to modify his name to "Jeff"
Then it should not be successful
And the employee's name should be "Jeff Alln Johnson William"

Scenario: Can modify 4 names to 2
Given an active employee
And the employee's name is "Jeff Alln Johnson William"
When I attempt to modify his name to "Jeff William"
Then the employee's name should be "Jeff William"

Scenario: Modify Display Name
Given an active employee
And the employee's "display name" is "Bozo"
When I modify his "display name" to "Zonkers"
Then the employee's "display name" should be "Zonkers"

Scenario: Modify Phone Number
Given an active employee
And the employee's "phone number" is "123"
When I modify his "phone number" to "456"
Then the employee's "phone number" should be "456"

Scenario: Modify Company Email
Given an active employee
And the employee's "company email" is "a@b.com"
When I modify his "company email" to "d@j.com"
Then the employee's "company email" should be "d@j.com"

Scenario: Modify Personal Email
Given an active employee
And the employee's "personal email" is "a@b.com"
When I modify his "personal email" to "d@j.com"
Then the employee's "personal email" should be "d@j.com"

Scenario: Modify Tags
Given an active employee
And the employee's "tags" is "abc"
When I modify his "tags" to "kjh"
Then the employee's "tags" should be "kjh"

Scenario: Modify Address
Given an active employee
And the employee's "address" is "123 Mockingbird"
And the employee's "city" is "Nashville"
And the employee's "region" is "South"
And the employee's "country" is "USA"
When I modify his address to "3001 Barrio Los Encuentros,Santa Ana,FM,Honduras"
Then the employee's "address" should be "3001 Barrio Los Encuentros"
And the employee's "city" should be "Santa Ana"
And the employee's "region" should be "FM"
And the employee's "country" should be "Honduras"