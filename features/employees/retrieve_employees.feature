Feature: Retrieve a Employees

In order to ___ as a ______ I can ______.

Scenario: Get Employees by Id
Given an active employee
When I retrieve an employee by the Id
Then I should get the same employee

Scenario: Find Employees by Last Name
Given an active employee
And the employee's name is "Jeff Alln Johnson William"
And an active employee
And the employee's name is "Matthew Johnson"
And an active employee
And the employee's name is "Byron Sommardahl"
When I retrieve employees where "last name" is "Johnson"
Then there should be 2 employees

Scenario: Find Employees by Middle Name
Given an active employee
And the employee's name is "Jeff Allen Johnson William"
And an active employee
And the employee's name is "Matthew Johnson"
And an active employee
And the employee's name is "Byron Allen Sommardahl"
When I retrieve employees where "middle name" is "Allen"
Then there should be 2 employees