Feature: Create an Employee

In order to access my employee later as a member of peopleops I can create an employee.

Scenario: Create a New Employee
Given there are no employees with name "John Philip Williams"
When I create an employee with name "John Philip Williams"
Then the employee's "first" name should be "John"
And the employee's "middle" name should be "Philip"
And the employee's "last" name should be "Williams"

Scenario: Can create employee with similar name
Given an active employee 
And the employee's name is "Ralph Waldo Emmerson"
When I create an employee with name "Ralph Waldo Emmerson Maya"
Then the employee should exist

Scenario: Attempt to Create Duplicate Employee
Given an active employee 
And the employee's name is "Ralph Waldo Emmerson"
When I attempt to create an employee with name "Ralph Waldo Emmerson"
Then it should not be successful

Scenario: Should not be able to create an employee without last name
Given I want to create a new employee
When I attempt to create an employee with name "Ralph"
Then it should not be successful
