const axios = require("axios");
const https = require("https");

class EmployeeApi {

    constructor(baseURL, headers) {
        this.http = axios.create({
            baseURL,
            timeout: 10000,
            headers,
            httpsAgent: new https.Agent({
                rejectUnauthorized: false
            })
        });
    }

    async create(employee) {
        var result = await this.request("post", ``, employee);        
        return result;
    }

    async changeNames(id, names) {
        var result = await this.request("put", `${id}/names`, names);            
        return result;
    }

    async changeSomething(prop, id, payload) {
        var result = await this.request("put", `${id}/${prop}`, payload);            
        return result;
    }

    async getAll(conditions){
        var conditionString = Object.keys(conditions).map(key=> `${key}=${conditions[key]}`).join("&");
        var result = await this.request("get", `?${conditionString}`);
        return result;
    }

    async getOne(id){
        var result = await this.request("get", id);
        return result;
    }

    async remove(id){
        var result = await this.request("delete", id);
        return result;
    }

    async request(verb, route, payload) {
        try {

            var result = await this.http[verb](route, payload);
            return result.data;
        }
        catch (error) {
            throw {
                message: `${verb.toUpperCase()} request resulted in an error. ${error.response.statusText}. ${error.response.data.error}`,
                status: error.response.status,
                method: error.config.method,
                url: error.config.url,
                payload: error.config.data
            };
        }
    }
}
module.exports = {
    EmployeeApi
};