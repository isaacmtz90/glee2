// const { setWorldConstructor } = require("cucumber");

const { Crud } = require("../../crud/crud");
const { EmployeeApi } = require("./employeeApi");

class EmployeeWorld {
    constructor() {
        this.currentEmployeeId = false;
        this.currentError = false;
        this.currenttPayload = false;
        this.crud = new Crud("https://localhost:5001/crud/employees");
        this.employees = new EmployeeApi("https://localhost:5001/api/employees");
    }

    setCurrentEmployeeId(guid) {
        this.currentEmployeeId = guid;
    }

    setCurrentError(error){
        this.currentError = error;
    }

    setCurrentPayloaad(payload){
        this.currenttPayload = payload;
    }
}

module.exports = {
    EmployeeWorld
};

// setWorldConstructor(CustomWorld);