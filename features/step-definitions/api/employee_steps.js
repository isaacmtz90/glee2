const { Given, Then, When, setWorldConstructor, setDefaultTimeout, After, Before } = require("cucumber");
const { EmployeeWorld } = require("./support/employeeWorld");
const { expect } = require("chai");
const { parseNames, getCamelCaseProperty, generateGuid } = require("./support/helpers");

setWorldConstructor(EmployeeWorld);
setDefaultTimeout(60 * 1000);

Before(async function () {
    await this.crud.clearAll();
});

Given("I want to create a new employee", () => {
    return;
});

Given("an active employee", async function () {
    var id = generateGuid();

    await this.crud.create({
        id,
        firstName: "Test",
        middleName: "Testing",
        lastName: "Testingly",
        birthDate: "1-1-1990",
        companyEmail: `test+${new Date().getDate()}@test.com`
    });

    this.setCurrentEmployeeId(id);
});

Then("the employee should exist", async function () {
    var emp = await this.crud.find(this.currentEmployeeId);
    expect(emp.id).to.equal(this.currentEmployeeId);
});

Given("the employee's name is {string}", async function (arg1) {
    var names = parseNames(arg1);
    await this.crud.modify(this.currentEmployeeId, names);
});

async function modifyNames(world, name) {
    var names = parseNames(name);
    await world.employees.changeNames(world.currentEmployeeId, names);
}

When("I modify his name to {string}", async function (name) {
    await modifyNames(this, name);
});

When("I attempt to modify his name to {string}", async function (name) {
    try {
        await modifyNames(this, name);
    }
    catch (error) {
        this.setCurrentError(error);
    }
});

When("I modify his address to {string}", async function (newAddressWithCommas) {
    var addressParts = newAddressWithCommas.split(",");
    var changedAddress = {
        address: addressParts[0],
        city: addressParts[1],
        region: addressParts[2],
        country: addressParts[3]
    };
    await this.employees.changeSomething("address", this.currentEmployeeId, changedAddress);
});

Then("the employee's {string} name should be {string}", async function (whichName, expectedName) {
    var foundEmployee = await this.crud.find(this.currentEmployeeId);
    if (whichName === "second last") whichName = "secondLast";
    expect(foundEmployee[`${whichName}Name`]).to.equal(expectedName);
});

Then("the employee's name should be {string}", async function (name) {
    var names = parseNames(name);
    var foundEmployee = await this.crud.find(this.currentEmployeeId);
    var foundEmployeeNames = {
        firstName: foundEmployee.firstName,
        middleName: foundEmployee.middleName,
        lastName: foundEmployee.lastName,
        secondLastName: foundEmployee.secondLastName
    };
    expect(foundEmployeeNames).to.deep.equal(names);
});

async function createEmployeeWithName(world, name) {
    var names = parseNames(name);
    var employee = await world.employees.create({
        firstName: names.firstName,
        middleName: names.middleName,
        lastName: names.lastName,
        secondLastName: names.secondLastName,
        birthDate: "1-1-1990",
        email: `test+${new Date().getDate()}@test.com`
    });

    world.setCurrentEmployeeId(employee.id);
}

When('I create an employee with name {string}', async function (name) {
    await createEmployeeWithName(this, name);
});

When('I attempt to create an employee with name {string}', async function (name) {
    try {
        await createEmployeeWithName(this, name);
    }
    catch (error) {
        this.setCurrentError(error);
        this.setCurrentEmployeeId(false);
    }
});

Then('it should not be successful', async function () {
    expect(this.currentError).to.not.be.undefined;
    expect(this.currentError).to.not.be.false;
    expect(this.currentError.status).to.equal(400);
});

Given('there are no employees with name {string}', function (string) {
    //do nothing
});

When('I retrieve employees where {string} is {string}', async function (propertyNameWithSpaces, value) {
    var camelCaseProp = getCamelCaseProperty(propertyNameWithSpaces);
    var payload = {};
    payload[camelCaseProp] = value;
    var emps = await this.employees.getAll(payload);
    this.setCurrentPayloaad(emps);
});

Then('there should be {int} employees', function (expectedEmployeeCount) {
    expect(this.currenttPayload).to.be.lengthOf(2);
});

Then('the employee\'s {string} should be {string}', async function (propertyNameWithSpaces, value) {
    var foundEmployee = await this.crud.find(this.currentEmployeeId);
    var prop = getCamelCaseProperty(propertyNameWithSpaces);
    expect(foundEmployee[prop]).to.equal(value);
});

async function modifyProp(world, propertyNameWithSpaces, value) {
    var camelCaseProp = getCamelCaseProperty(propertyNameWithSpaces);
    var payload = {};
    payload[camelCaseProp] = value;
    await world.employees.changeSomething(camelCaseProp, world.currentEmployeeId, payload);
}

When('I attempt to modify his {string} to {string}', async function (propertyNameWithSpaces, value) {
    try {
        await modifyProp(this, propertyNameWithSpaces, value);
    }
    catch (error) {
        this.setCurrentError(error);
        this.setCurrentEmployeeId(false);
    }
});

When('I modify his {string} to {string}', async function (propertyNameWithSpaces, value) {
    await modifyProp(this, propertyNameWithSpaces, value);
});

Given('the employee\'s {string} is {string}', async function (propertyNameWithSpaces, value) {
    var propName = getCamelCaseProperty(propertyNameWithSpaces);
    var payload = {};
    payload[propName] = value;
    await this.crud.modify(this.currentEmployeeId, payload);
});

When('I retrieve an employee by the Id', async function () {
    var emp = await this.employees.getOne(this.currentEmployeeId);
    this.setCurrentPayloaad(emp);
});

Then('I should get the same employee', function () {
    expect(this.currenttPayload.id).to.be.equal(this.currentEmployeeId);
});

When('I remove an employee by the Id', async function () {
    await this.employees.remove(this.currentEmployeeId);
});

Then('I not should be able to retrieve the same employee', async function () {
    var emp = await this.crud.find(this.currentEmployeeId);
    expect(emp.removed).to.be.true;
});