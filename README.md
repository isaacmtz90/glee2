# Welcome

## CI/CD

This repository is setup with a fill CI/CD pipeline through Gitlab and AWS.

### Review Apps

When you create a branch, Gitlab will kick off a build and eventually spin up a dedicated and temporary environment for that branch (frontend in S3, backend in EB). It will also report back to the corresponding merge request with URL's for frontend and backend. Your QA team and others will be able to test your feature branch in isolation and fully verify, validate and approve before merging back in to the origin branch.

TODO: Spin up and migrate temporary database in RDS to match frontend and backend.
TODO: Detect changes in frontend or backend and only deploy review if necessary.

### Setup

Before you start you need a few things first:

- 3 Auth0 tenants one for each environment (dev, staging and prod)
- 3 ECR repository to store the docker images
- 3 RDS instances, for dev(review apps), staging and prod

Once you have all the thing mentioned above you should be able to
Ensure the following environment variables are present in your CI/CD configuration in Gitlab.

```
AWS_ACCESS_KEY_ID=something
AWS_SECRET_ACCESS_KEY=something
AWS_ACCESS_KEY_ID_CLIENT=something # Access key id that belongs to the client aws account
AWS_SECRET_ACCESS_KEY_CLIENT=something
AWS_REGION=us-east-1
AWS_ECR_REGISTRY=something # ECR used to store the review apps docker images
AWS_ECR_REGISTRY_STAGING=something
AWS_ECR_REGISTRY_PROD=something
EB_ENV_NAME_PREFIX=project-prefix # This is used to generate the review backend apps name.
EB_ENV_NAME_STAGING=eb-environment-name-for-staging
EB_ENV_NAME_PROD=eb-environment-name-for-production
NODE_ENV=production
S3_BUCKET_NAME_PREFIX=project-prefix # This is used to generate the review frontend apps name.
S3_BUCKET_NAME_STAGING=bucket-for-front-end-staging
S3_BUCKET_NAME_PROD=bucket-for-front-end-production
TYPEORM_CONNECTION=postgres # Allowed values: https://github.com/typeorm/typeorm/blob/master/docs/connection-options.md#common-connection-options
TYPEORM_DATABASE=dbname
TYPEORM_HOST=pgdbhost.name.com
TYPEORM_USERNAME=username
TYPEORM_PASSWORD=password
TYPEORM_PORT=5432
AUTH0_CLIENT_ID_DEV=something
AUTH0_DOMAIN_DEV=something
```
